= Expression Evaluator

Tool to evaluate ad-hoc pseudo-mathematical expressions.

CAUTION:: This is a pre-alpha preview, so expect it to be a little messy in the short term, *and don't use it for production software yet*!

== TODO

* Correctly implement error handling:
** FAST/SAFE semantics (implemented without _recoverable_ logging),
** Recoverable logging (only exists as HRable, collected sys-out)
** integration into factory?
** remaining tests?
* Do subtraction substitution properly.
* Implement pre-evaluation validation (consider EHandle too?).
** Validation should involve:
*** checking that all values conform to either CONSTANT or VARIABLE
**** if a symbol or space exists, it means either the statement is malformed or an operator is not registered.
*** asserting all variables are either bound or defined
*** ensuring that all stated functions are registered.
* Add variable binding mechanism (via factory).
* Multi-param function-parsing support

== Expression Handling

====

EXPRESSION = EXPRESSION OPERATOR EXPRESSION |
             FUNCTION |
             ( EXPRESSION ) |
             VALUE

OPERATOR = _ --> defined, subtitutes to function

FUNCTION = \w+\( EXPRESSION \)

VALUE = CONSTANT | VARIABLE

CONSTANT = \-\d+\.\d*

VARIABLE = \w+(^\()

====
