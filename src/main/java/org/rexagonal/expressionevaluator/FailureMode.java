package org.rexagonal.expressionevaluator;

public enum FailureMode {
    //With this behaviour:
    //- Evaluation is expected to halt immediately with exception
    //- Errors are to be used for immediate debug.
    //- Upstream execution will fail, but bad data propagation will be averted with complete certainty.
    FAST,
    //With this behaviour:
    //- Evaluation is to return empty, with errors.
    //- Errors are to be used for possible automated remediation.
    //- Upstream execution will continue; upstream processes will determine what occurs next, but no misleading numbers are produced.
    SAFE,
    //With this behaviour:
    //- Evaluation will carry on, with dummy values.
    //- Errors are to be used for future debug.
    //- Upstream execution will continue with guesstimated values; errors will be presented, but any error in data will be propagated if ignored.
    WILD
}
