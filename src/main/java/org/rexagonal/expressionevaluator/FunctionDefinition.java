package org.rexagonal.expressionevaluator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public class FunctionDefinition {
    public enum Type {
        UNARY, BINARY, WRAPPING
    }

    private final String name;
    private final Function<BigDecimal[], BigDecimal> function;
    private final Optional<String> symbol;
    private final Optional<String> escapedRegexSymbol;
    private final Type functionType;

    public FunctionDefinition(String name, String symbol, String escapedRegexSymbol, Type functionType, Function<BigDecimal[], BigDecimal> function) {
        this.name = name;
        this.function = function;
        this.symbol = Optional.ofNullable(symbol);
        this.escapedRegexSymbol = Optional.ofNullable(escapedRegexSymbol);
        this.functionType = functionType;
    }

    public FunctionDefinition(String name, String symbol, Type functionType, Function<BigDecimal[], BigDecimal> function) {
        this(name, symbol, symbol, functionType, function);
    }

    public FunctionDefinition(String name, Function<BigDecimal[], BigDecimal> function) {
        this(name, null, null, Type.WRAPPING, function);
    }

    String getName() {
        return name;
    }

    Function<BigDecimal[], BigDecimal> getFunction() {
        return function;
    }

    Optional<String> getSymbol() {
        return symbol;
    }

    String getEscapedRegexSymbol() {
        return escapedRegexSymbol.get();
    }

    Type getFunctionType() {
        return functionType;
    }

    static List<FunctionDefinition> standardDefinitions() {
        List<FunctionDefinition> standardDefinitions = new ArrayList<>();
        standardDefinitions.add(new FunctionDefinition("sum", "+", "\\+", Type.BINARY, inputs ->
                Stream.of(inputs).reduce(BigDecimal.ZERO, (bd1, bd2) -> bd1.add(bd2))));
        standardDefinitions.add(new FunctionDefinition("product", "*", "\\*", Type.BINARY, inputs ->
                Stream.of(inputs).reduce(BigDecimal.ONE, (bd1, bd2) -> bd1.multiply(bd2))));
        standardDefinitions.add(new FunctionDefinition("division", "/", Type.BINARY, inputs ->
                Stream.of(inputs).reduce((bd1, bd2) -> bd1.divide(bd2)).orElse(BigDecimal.ZERO)));
        standardDefinitions.add(new FunctionDefinition("pow", "^", "\\^", Type.BINARY, inputs -> {
            List<BigDecimal> inputList = Arrays.asList(inputs);
            Collections.reverse(inputList);
            return inputList.stream().reduce(BigDecimal.ONE, (bd1, bd2) -> bd2.pow(bd1.intValue()));
        }));
        return standardDefinitions;
    }
}
