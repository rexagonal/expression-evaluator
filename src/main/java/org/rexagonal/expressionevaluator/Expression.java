package org.rexagonal.expressionevaluator;

import org.rexagonal.expressionevaluator.exception.InsufficientDataForEvaluationException;
import org.rexagonal.expressionevaluator.exception.VariableNotBoundException;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Expression {

    private final ExpressionFactory factory;

    private Term rootTerm;
    private transient Map<String,Term> referencedTerms = new HashMap<>();
    private Optional<Function<String, BigDecimal>> variableSelector;

    private final FailureMode failureMode;
    private final List<String> errors;

    Expression(ExpressionFactory factory, String expressionString, FailureMode defaultFailureMode) {
        this.factory = factory;
        this.failureMode = defaultFailureMode;

        variableSelector = Optional.empty();

        // TODO: This replace should be made generic, and part of the FunctionDefinition...
        // TODO: To specific, may break on mixed terms or spaces still
        // TODO: Perhaps replace with a unary function type?
        if (expressionString.contains("(-") || expressionString.contains(",-") ) {
            expressionString = expressionString.replace("(-", "(0+-1*");
            expressionString = expressionString.replace(",-", ",0+-1*");
        } else {
            expressionString = expressionString.replace("-", "+-1*");
        }
        rootTerm = parseExpression(expressionString);
        errors = new ArrayList<>();
    }

    private Term parseExpression(String expressionString) {
        int bracketDepth = 0;
        int topLevelIndex = -1;

        for (int i = 0; i < expressionString.length(); i++) {
            if (expressionString.charAt(i) == '(') {
                if (bracketDepth == 0) {
                    topLevelIndex = i;
                }
                bracketDepth++;
            }

            if (expressionString.charAt(i) == ')') {
                bracketDepth--;

                if (bracketDepth == 0) {
                    String termReference = "$$" + new Random().nextInt(10000);
                    Term groupedTerm = parseExpression(expressionString.substring(topLevelIndex+1, i));

                    int j = 0;
                    StringBuilder sb = new StringBuilder();
                    while ( (topLevelIndex-++j >= 0) && (expressionString.charAt(topLevelIndex-j)+"").matches("[A-Za-z]") ) {
                        sb.append(expressionString.charAt(topLevelIndex-j));
                    }
                    String functionName = sb.reverse().toString();
                    if (!functionName.isEmpty()) {
                        groupedTerm = Term.builder(new ExpressionDelegate()).createAsEdge(functionName, Collections.singletonList(groupedTerm));
                    }

                    referencedTerms.put(termReference, groupedTerm);
                    expressionString = expressionString.substring(0, topLevelIndex-j+1) +
                            termReference +
                            expressionString.substring(i+1)
                    ;

                    topLevelIndex = -1;
                    i = 0;
                }
            }
        }

        for (FunctionDefinition definition : factory.listDefinitions()) {
            if (definition.getSymbol().map(expressionString::contains).orElse(false)) {
                List<Term> moo = Stream.of(expressionString.split(definition.getEscapedRegexSymbol()))
                        .map(this::parseExpression)
                        .collect(Collectors.toList());

                return Term.builder(new ExpressionDelegate()).createAsEdge(definition.getName(), moo);
            }
        }

        if (referencedTerms.containsKey(expressionString)) {
            return referencedTerms.get(expressionString);
        }

        return Term.builder(new ExpressionDelegate())
                .createAsLeaf(expressionString);
    }

    public String listRequiredVars() {
        return rootTerm.listVariableNames().stream().collect(Collectors.joining(", "));
    }

    public boolean isExpressionValid() {
        /*
         * Validation should involve:
         * a) checking that all values conform to either CONSTANT or VARIABLE
         * b) if a symbol or space exists, it means either the statement is malformed or an operator is not registered.
         * c) asserting all variables are either bound or defined
         */

        // Do all roots resolve to constant or variable
        boolean areExpressionLeavesValid = rootTerm.areTermLeavesValid();

        return areExpressionLeavesValid;
    }

    public String displayRepresentation() {
        return rootTerm.displayRepresentation();
    }

    private Result evaluate(Optional<Function<String, BigDecimal>> variables) {
        this.variableSelector = variables;

        if (!isExpressionValid() && FailureMode.FAST.equals(failureMode)) {
            return new Result(Collections.singletonList("Equation validation failed!"));
        }

        return new Result(rootTerm.evaluate(), errors);
    }

    public Result evaluate(Function<String, BigDecimal> variables) {
        return evaluate(Optional.of(variables));
    }

    public Result evaluate() {
        return evaluate(Optional.empty());
    }

    class ExpressionDelegate {

        Function<BigDecimal[], BigDecimal> getFunctionForName(String name) {
            return factory.getFunctionForName(name);
        }

        Optional<BigDecimal> assignVariable(String name) {
            if (!variableSelector.isPresent()) {
                String errorMessage = MessageFormat.format("Unbound variable: No variables selector has been defined for {0}", name);
                logError(errorMessage);
            }

            Function<String,BigDecimal> rawSelector;
            switch (failureMode) {
                case FAST:
                    rawSelector = variableSelector.orElseThrow(VariableNotBoundException::new);
                    return Optional.ofNullable(rawSelector.apply(name));
                case WILD:
                    rawSelector = variableSelector.orElse(variableSelector -> BigDecimal.ZERO);
                    return Optional.ofNullable(rawSelector.apply(name));
                case SAFE:
                default:
                    return variableSelector.map(func -> func.apply(name));
            }
        }

        void logError(String error) {
            errors.add(error);
            System.err.println(error);
        }

        public FailureMode getFailureMode() {
            return failureMode;
        }
    }

}
