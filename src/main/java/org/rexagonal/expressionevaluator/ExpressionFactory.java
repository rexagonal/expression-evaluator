package org.rexagonal.expressionevaluator;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

public class ExpressionFactory {
    private final Map<String, Function<BigDecimal[], BigDecimal>> functionMap;
    private final List<FunctionDefinition> functionDefinitionList;

    private FailureMode defaultFailureMode;

    private ExpressionFactory() {
        functionDefinitionList = FunctionDefinition.standardDefinitions();
        functionMap = new TreeMap<>();
        functionDefinitionList.forEach(definition -> functionMap.put(definition.getName(), definition.getFunction()));

        defaultFailureMode = FailureMode.SAFE;
    }

    public ExpressionFactory setDefaultFailureMode(FailureMode mode) {
        this.defaultFailureMode = mode;
        return this;
    }

    public ExpressionFactory registerFunction(String name, Function<BigDecimal[], BigDecimal> function) {
        registerFunction(new FunctionDefinition(name, function));
        return this;
    }

    public ExpressionFactory registerFunction(FunctionDefinition definition) {
        functionDefinitionList.add(definition);
        functionMap.put(definition.getName(), definition.getFunction());
        return this;
    }

//    public ExpressionFactory registerSimpleVariableBinding(String variableName) {
//
//    }

    public Expression marshallExpression(String expressionString) {
        return new Expression(this, expressionString, defaultFailureMode);
    }

    Function<BigDecimal[], BigDecimal> getFunctionForName(String name) {
        return functionMap.get(name);
    }

    Collection<FunctionDefinition> listDefinitions() {
        return functionDefinitionList;
    }

    public static ExpressionFactory create() {
        return new ExpressionFactory();
    }
}
