package org.rexagonal.expressionevaluator;

import org.rexagonal.expressionevaluator.exception.FunctionNotRegisteredException;
import org.rexagonal.expressionevaluator.exception.InsufficientDataForEvaluationException;
import org.rexagonal.expressionevaluator.exception.MalformedTermException;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

class Term {
    enum Type { FUNCTION, VARIABLE, CONSTANT, INDETERMINATE }

    private final Builder builder;
    private final String operationName;
    private String functionName;
    private Function<BigDecimal[], BigDecimal> functionTemplate;

    private Optional<String> constantVar;
    private List<Term> downstreamTerms;
    private Type termType = Type.INDETERMINATE;

    private Term(Builder builder, List<Term> downstreamTerms) {
        this.builder = builder;
        this.operationName = builder.getOperationName();
        this.downstreamTerms = downstreamTerms;
        constantVar = Optional.empty();

        functionName = builder.operationName;
        functionTemplate = builder.getExpressionRoot().getFunctionForName(functionName);
        termType = Type.FUNCTION;
    }

    private Term(Builder builder, String value) {
        this.builder = builder;
        this.operationName = builder.operationName;
        this.downstreamTerms = Collections.emptyList();
        this.constantVar = Optional.of(value);

        functionName = builder.operationName;
        functionTemplate = i -> i[0];
        if (isValueConstant(constantVar)) {
            termType = Type.CONSTANT;
        } else if (isValueVariable(constantVar)) {
            termType = Type.VARIABLE;
        }
    }

    Optional<BigDecimal> evaluate() {
        switch (termType) {
            case CONSTANT:
                return Optional.of(new BigDecimal(constantVar.get()));
            case VARIABLE:
                return handleVariablePopulation();
            case FUNCTION:
                return handleFunctionEvaluation();
            default:
                return handleIndeterminateTermFailure();
        }
    }

    private Optional<BigDecimal> handleVariablePopulation() {
        return builder.getExpressionRoot().assignVariable(constantVar.get());
    }

    private Optional<BigDecimal> handleIndeterminateTermFailure() {
        Expression.ExpressionDelegate root = builder.getExpressionRoot();
        String errorMessage = MessageFormat.format("Term form was a leaf that was neither number nor identifier: {0}", constantVar.orElse("null"));
        root.logError(errorMessage);
        switch (root.getFailureMode()) {
            case FAST:
                throw new MalformedTermException();
            case WILD:
                return Optional.of(BigDecimal.ZERO);
            case SAFE:
            default:
                return Optional.empty();
        }
    }

    private Optional<BigDecimal> handleFunctionEvaluation() {
        List<Optional<BigDecimal>> downstreamResults = downstreamTerms.stream()
                .map(Term::evaluate)
                .collect(Collectors.toList());

        for (Optional<BigDecimal> result : downstreamResults) {
            if (!result.isPresent()) {
                return Optional.empty();
            }
        }

        if (functionTemplate == null) {
            Expression.ExpressionDelegate root = builder.getExpressionRoot();
            String errorMessage = MessageFormat.format("Unable to evaluate function {0}, reverting to passthrough: FRAGMENT[{1}].", functionName, displayRepresentation());
            root.logError(errorMessage);
            switch (root.getFailureMode()) {
                case FAST:
                    throw new FunctionNotRegisteredException();
                case SAFE:
                    return Optional.empty();
                case WILD:
                default:
                    functionTemplate = values -> values[0];
            }

        }

        return Optional.of(functionTemplate.apply(downstreamResults.stream().map(Optional::get).toArray(BigDecimal[]::new)));
    }

    private boolean isValueConstant(Optional<String> value) {
        return value.orElse("").matches("-?[\\d]+.?[\\d]*");
    }

    private boolean isValueVariable(Optional<String> value) {
        return value.orElse("").matches("[a-z][a-z0-9]*[\\.a-z0-9]*");
    }

    String displayRepresentation() {
        StringBuilder sb = new StringBuilder();

        if (operationName != null) {
            sb.append(operationName);
        }

        sb.append("(");
        sb.append(downstreamTerms.stream()
                .map(Term::displayRepresentation)
                .collect(Collectors.joining(", ")));
        sb.append(constantVar.orElse(""));
        sb.append(")");
        return sb.toString();
    }

    List<String> listVariableNames() {
        List<String> upstreamVars = new ArrayList<>();
        upstreamVars.addAll(
                downstreamTerms.stream()
                        .map(Term::listVariableNames)
                        .flatMap(List::stream)
                        .collect(Collectors.toList()));
        if (isValueVariable(constantVar)) {
            constantVar.map(upstreamVars::add);
        }
        return upstreamVars;
    }

    public Boolean areTermLeavesValid() {
        switch (termType) {
            case CONSTANT:
            case VARIABLE:
                return true;
            case FUNCTION:
                return downstreamTerms.stream()
                        .map(Term::areTermLeavesValid)
                        .reduce(Boolean.TRUE, (a,b) -> a && b);
            case INDETERMINATE:
            default:
                return false;
        }
    }

    static Builder builder(Expression.ExpressionDelegate expressionRoot) {
        return new Builder(expressionRoot);
    }

    static class Builder {
        private final Expression.ExpressionDelegate expressionRoot;
        private String operationName;
        private List<Term> downstreamTerms = new ArrayList<>();

        private Builder(Expression.ExpressionDelegate expressionRoot) {
            this.expressionRoot = expressionRoot;
        }

        public Builder addToEdge(Term termToAdd) {
            downstreamTerms.add(termToAdd);
            return this;
        }

        public Term createAsEdge(String operationName) {
            this.operationName = operationName;
            return new Term(this, downstreamTerms);
        }

        public Term createAsEdge(String operationName, List<Term> terms) {
            downstreamTerms.addAll(terms);
            return createAsEdge(operationName);
        }

        public Term createAsLeaf(String value) {
            this.operationName = "leaf";
            return new Term(this, value);
        }

        public String getOperationName() {
            return operationName;
        }

        public Expression.ExpressionDelegate getExpressionRoot() {
            return expressionRoot;
        }
    }
}
