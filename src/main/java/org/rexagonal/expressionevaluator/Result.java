package org.rexagonal.expressionevaluator;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public class Result {
    private Optional<BigDecimal> value;
    private List<String> runtimeErrors;

    public Result(Optional<BigDecimal> value, List<String> runtimeErrors) {
        this.value = value;
        this.runtimeErrors = runtimeErrors;
    }

    public Result(List<String> runtimeErrors) {
        this.value = Optional.empty();
        this.runtimeErrors = runtimeErrors;
    }

    public Optional<BigDecimal> getValue() {
        return value;
    }

    public List<String> getRuntimeErrors() {
        return runtimeErrors;
    }
}
