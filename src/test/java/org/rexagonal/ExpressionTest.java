package org.rexagonal;

import org.junit.BeforeClass;
import org.junit.Test;
import org.rexagonal.expressionevaluator.Expression;
import org.rexagonal.expressionevaluator.ExpressionFactory;
import org.rexagonal.expressionevaluator.Result;

import java.math.BigDecimal;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class ExpressionTest {
    public static ExpressionFactory factory;

    @BeforeClass
    public static void setupFactory() {
        factory = ExpressionFactory.create()
                .registerFunction("fun", inputs -> inputs[0].add(BigDecimal.TEN));
    }

    //--- Real tests start here ---//

    @Test
    public void doesGeneralAdditionSubtractionResolveCorrectly() {
        //Given
        String tester = "a+b-1+2+3-c-d+1+a";

        //When
        Expression e = factory.marshallExpression(tester);
        Result result = e.evaluate(name -> {
                    switch(name) {
                        case "a": return BigDecimal.ONE;
                        case "b": return BigDecimal.TEN;
                        case "c": return BigDecimal.valueOf(3);
                        case "d": return BigDecimal.valueOf(3.5);
                    }

                    return BigDecimal.ZERO;
                });

        //Then
        assertThat(result.getValue().get(), is(BigDecimal.valueOf(10.5)));
    }

    @Test
    public void doesMultiplicationResolveCorrectly() {
        //Given
        String tester = "1*alpha*3*beta*gamma*2";

        //When
        Expression e = factory.marshallExpression(tester);
        Result result = e.evaluate(name -> {
            switch(name) {
                case "alpha": return BigDecimal.ONE;
                case "beta": return BigDecimal.TEN;
                case "gamma": return BigDecimal.valueOf(3);
            }

            return BigDecimal.ZERO;
        });

        //Then
        assertThat(result.getValue().get(), is(BigDecimal.valueOf(180)));
    }

    @Test
    public void doesIntraPowerPrecedenceResolveCorrectly() {
        //Given
        // (4^3)^2 = 4096 [64^2] [incorrect]
        // 4^(3^2) = 262144 [4^9] [correct]
        String tester = "4^3^2";

        //When
        Expression e = factory.marshallExpression(tester);
        Result result = e.evaluate();

        //Then
        assertThat(result.getValue().get(), is(BigDecimal.valueOf(262144)));
    }

    @Test
    public void doesIntraDividePrecedenceResolveCorrectly() {
        //Given
        // 10/(5/2) = 4 [10/0.4] [incorrect]
        // 10/5/2 = 1 [2/2] [correct]
        String tester = "10/5/2";

        //When
        Expression e = factory.marshallExpression(tester);
        Result result = e.evaluate();

        //Then
        assertThat(result.getValue().get(), is(BigDecimal.valueOf(1)));
    }

    @Test
    public void doesPrecedenceWorkWithoutBrackets() {
        String tester = "a+b*c/d^e";
        // a+(b*(c/(d^e)))
        // a+(b*(c/(8)))
        // a+(b*(0.5))
        // a+(3.5)
        // 13.5

        Expression e = factory.marshallExpression(tester);
        Result result = e.evaluate(name -> {
            switch(name) {
                case "a": return BigDecimal.TEN;
                case "b": return BigDecimal.valueOf(7);
                case "c": return BigDecimal.valueOf(4);
                case "d": return BigDecimal.valueOf(2);
                case "e": return BigDecimal.valueOf(3);
            }

            return BigDecimal.ZERO;
        });

        //Then
        assertThat(result.getValue().get(), is(BigDecimal.valueOf(13.5)));

    }

    @Test
    public void doesPrecedenceWorkWithSimpleBrackets() {
        String tester = "a1+(b1*c1)/d1^(e1-1)";
        // a+((b*c)/(d^(e-1)))
        // a+((b*c)/(d^(2)))
        // a+((28)/(4))
        // a+(7)
        // 17

        Expression e = factory.marshallExpression(tester);
        Result result = e.evaluate(name -> {
            switch(name) {
                case "a1": return BigDecimal.TEN;
                case "b1": return BigDecimal.valueOf(7);
                case "c1": return BigDecimal.valueOf(4);
                case "d1": return BigDecimal.valueOf(2);
                case "e1": return BigDecimal.valueOf(3);
            }

            return BigDecimal.ZERO;
        });

        //Then
        assertThat(result.getValue().get(), is(BigDecimal.valueOf(17)));
    }

    @Test
    public void parsesFunctionsCorrectly() {
        String tester = "fun(2+2)/fun(-12)";

        Expression e = factory.marshallExpression(tester);
        Result result = e.evaluate(name -> BigDecimal.ONE);

        System.out.println(e.displayRepresentation());
        assertThat(result.getValue().get(), is(BigDecimal.valueOf(-7)));
    }

    @Test
    public void doesPrecedenceWorkRecursivelyWithBrackets() {
        String tester = "(a2+(b2*c2))/d2^(e2-1)";
        // (a+((28))/(d^(2)))
        // (38)/(4)
        // 9.5

        Expression e = factory.marshallExpression(tester);
        Result result = e.evaluate(name -> {
            switch(name) {
                case "a2": return BigDecimal.TEN;
                case "b2": return BigDecimal.valueOf(7);
                case "c2": return BigDecimal.valueOf(4);
                case "d2": return BigDecimal.valueOf(2);
                case "e2": return BigDecimal.valueOf(3);
            }

            return BigDecimal.ZERO;
        });

        //Then
        assertThat(result.getValue().get(), is(BigDecimal.valueOf(9.5)));
    }

    //TODO: make into parameterised test, of different permutations?
    @Test
    public void canPerformGeneralEvaluation1() {
        String tester = "50-a*(b+c^d)/1+e*(fun(f+g-a1+a2)-h*(i+j))+(k*l)^m";

        Expression e = factory.marshallExpression(tester);

        Result result = e.evaluate(name -> {
            switch(name) {
                case "a": return BigDecimal.ONE;
                case "b": return BigDecimal.ONE;
                case "c": return BigDecimal.ONE;
                case "d": return BigDecimal.ONE;
                case "m": return BigDecimal.ONE;
            }

            return BigDecimal.ZERO;
        });

        assertThat(result.getValue().get(), is(BigDecimal.valueOf(48l)));
    }

    //

    @Test
    public void areVariablesCorrectlyIdentified() {
        String tester = "50-a*(b+c^d)+7*(fun(f+var.prop-a1+a2)-h*(1+j))+(k*5)^m";

        Expression e = factory.marshallExpression(tester);

        assertThat(e.listRequiredVars(), is("a, b, c, d, f, var.prop, a1, a2, h, j, k, m"));
    }
}