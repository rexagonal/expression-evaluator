package org.rexagonal;

import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.rexagonal.expressionevaluator.Expression;
import org.rexagonal.expressionevaluator.ExpressionFactory;
import org.rexagonal.expressionevaluator.FailureMode;
import org.rexagonal.expressionevaluator.Result;
import org.rexagonal.expressionevaluator.exception.FunctionNotRegisteredException;
import org.rexagonal.expressionevaluator.exception.VariableNotBoundException;

import java.math.BigDecimal;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class FastFailureExpressionTest {
    public static ExpressionFactory factory;

    @BeforeClass
    public static void setupFactory() {
        factory = ExpressionFactory.create()
                .setDefaultFailureMode(FailureMode.FAST)
                .registerFunction("fun", inputs -> inputs[0].add(BigDecimal.TEN));
    }

    // Error-handling

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void areNonExistentFunctionsHandledFast() {
        thrown.expect(FunctionNotRegisteredException.class);

        String tester = "50-a*(b+c^d)+7*(pun(f+var.prop-a1+a2)-h*(1+j))+(k*5)^m";

        Expression e = factory.marshallExpression(tester);
        e.evaluate(name -> BigDecimal.ZERO);
    }

    @Test
    public void areUndefinedVariablesHandledFast() {
        thrown.expect(VariableNotBoundException.class);

        String tester = "50-a*(b+c^d)+7*(fun(f+var.prop-a1+a2)-h*(1+j))+(k*5)^m";

        Expression e = factory.marshallExpression(tester);
        e.evaluate();
    }
}