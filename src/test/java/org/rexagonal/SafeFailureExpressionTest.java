package org.rexagonal;

import org.junit.BeforeClass;
import org.junit.Test;
import org.rexagonal.expressionevaluator.Expression;
import org.rexagonal.expressionevaluator.ExpressionFactory;
import org.rexagonal.expressionevaluator.Result;

import java.math.BigDecimal;
import java.util.Optional;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class SafeFailureExpressionTest {
    public static ExpressionFactory factory;

    @BeforeClass
    public static void setupFactory() {
        factory = ExpressionFactory.create()
                .registerFunction("fun", inputs -> inputs[0].add(BigDecimal.TEN));
    }

    // Error-handling

    @Test
    public void areNonExistentFunctionsHandledSafely() {
        String tester = "50-a*(b+c^d)+7*(pun(f+var.prop-a1+a2)-h*(1+j))+(k*5)^m";

        Expression e = factory.marshallExpression(tester);
        Result result = e.evaluate(name -> BigDecimal.ZERO);

        assertThat(result.getValue(), is(Optional.empty()));
        assertThat(result.getRuntimeErrors().size(), is(1));
        assertThat(result.getRuntimeErrors().get(0), is("Unable to evaluate function pun, reverting to passthrough: FRAGMENT[pun(sum(leaf(f), leaf(var.prop), product(leaf(-1), leaf(a1)), leaf(a2)))]."));
    }

    @Test
    public void areUndefinedVariablesHandledSafely() {
        String tester = "50-a*(b+c^d)+7*(fun(f+var.prop-a1+a2)-h*(1+j))+(k*5)^m";

        Expression e = factory.marshallExpression(tester);
        Result result = e.evaluate();

        assertThat(result.getValue(), is(Optional.empty()));
        assertThat(result.getRuntimeErrors().size(), is(12));
//        assertThat(result.getRuntimeErrors().get(0), is());
    }
}