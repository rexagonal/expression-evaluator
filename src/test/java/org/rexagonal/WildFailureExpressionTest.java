package org.rexagonal;

import org.junit.BeforeClass;
import org.junit.Test;
import org.rexagonal.expressionevaluator.Expression;
import org.rexagonal.expressionevaluator.ExpressionFactory;
import org.rexagonal.expressionevaluator.FailureMode;
import org.rexagonal.expressionevaluator.Result;

import java.math.BigDecimal;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class WildFailureExpressionTest {
    public static ExpressionFactory factory;

    @BeforeClass
    public static void setupFactory() {
        factory = ExpressionFactory.create()
                .setDefaultFailureMode(FailureMode.WILD)
                .registerFunction("fun", inputs -> inputs[0].add(BigDecimal.TEN));
    }

    // Error-handling

    @Test
    public void areNonExistentFunctionsHandledAcceptably() {
        String tester = "50-a*(b+c^d)+7*(pun(f+var.prop-a1+a2)-h*(1+j))+(k*5)^m";

        Expression e = factory.marshallExpression(tester);
        Result result = e.evaluate(name -> BigDecimal.ZERO);

        assertThat(result.getValue().get(), is(BigDecimal.valueOf(51)));
        assertThat(result.getRuntimeErrors().size(), is(1));
        assertThat(result.getRuntimeErrors().get(0), is("Unable to evaluate function pun, reverting to passthrough: FRAGMENT[pun(sum(leaf(f), leaf(var.prop), product(leaf(-1), leaf(a1)), leaf(a2)))]."));
    }

    @Test
    public void areUndefinedVariablesHandledAcceptably() {
        String tester = "50-a*(b+c^d)+7*(fun(f+var.prop-a1+a2)-h*(1+j))+(k*5)^m";
        // 50-a*(b+c^d)+7*(fun(f+var.prop-a1+a2)-h*(1+j))+(k*5)^m
        // 50-a*(b+0^0)+7*(fun(0+0-0+0)-h*(1+0))+(0*5)^m
        // 50-a*(b+1)+7*(fun(0)-h*(1))+(0)^m
        // 50-a*(0+1)+7*(fun(0)-0*(1))+(0)^0
        // 50-a*(1)+7*(10-0)+1
        // 50-0+70+1
        // 121

        Expression e = factory.marshallExpression(tester);
        Result result = e.evaluate();

        assertThat(result.getValue().get(), is(BigDecimal.valueOf(121)));
        assertThat(result.getRuntimeErrors().size(), is(12));
    }
}